# Answers to exercise 2 questions
SELECT fname, lname FROM unidb_students AS s, unidb_attend AS a WHERE s.id = a.id AND a.dept = 'comp' AND a.num = 219;

SELECT fname, lname FROM unidb_students AS s, unidb_courses AS c  WHERE s.id = c.rep_id AND s.country != 'NZ';

SELECT office FROM unidb_lecturers AS l, unidb_courses AS c  WHERE l.staff_no = c.coord_no AND c.num =219;

SELECT DISTINCT s.fname, s.lname FROM unidb_students AS s, unidb_attend AS a , unidb_teach AS t, unidb_lecturers AS l WHERE s.id = a.id AND a.dept = t.dept AND a.num = t.num AND t.staff_no =  l.staff_no AND l.fname ='Te Taka';

SELECT s.id AS student_id, s.fname AS student_first_name, s.lname AS student_last_name, s.mentor AS mentor_ID, m.fname AS mentor_first_name, m.lname AS mentor_last_name FROM unidb_students AS s, unidb_students AS m WHERE s.mentor = m.id;

SELECT fname, lname FROM unidb_lecturers
  WHERE office LIKE "G.%"
UNION
  SELECT fname, lname FROM unidb_students
WHERE country != 'NZ';

SELECT staff_no AS ID, fname AS first_name, lname as last_name
FROM unidb_lecturers as l, unidb_courses as c
WHERE l.staff_no = c.coord_no AND c.dept ='comp' AND c.num = 219
UNION
SELECT id AS ID, fname AS first_name, lname as last_name
FROM unidb_students as s, unidb_courses AS c
WHERE s.id = c.rep_id AND  c.dept ='comp' AND c.num = 219;
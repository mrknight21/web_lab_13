# Answers to exercise 1 questions
SELECT dept FROM unidb_courses GROUP BY dept HAVING count(num) >= 1;

SELECT semester FROM unidb_attend GROUP BY semester HAVING count(id) >=1;

SELECT dept, num FROM unidb_attend GROUP BY dept, num HAVING count(id) >=1;

SELECT fname, lname, country FROM unidb_students ORDER BY fname;

SELECT fname, lname, mentor FROM unidb_students ORDER BY mentor;

SELECT * FROM unidb_lecturers ORDER BY office;

SELECT * FROM unidb_lecturers WHERE staff_no>500;

SELECT * FROM unidb_students WHERE id BETWEEN 1668 AND 1824;

SELECT * FROM unidb_students WHERE country IN ('US', 'NZ', 'AU');

SELECT * FROM unidb_lecturers WHERE office LIKE 'G.%';

SELECT * FROM unidb_courses WHERE dept != 'comp';

SELECT * FROM unidb_students WHERE country IN ('FR', 'MX');
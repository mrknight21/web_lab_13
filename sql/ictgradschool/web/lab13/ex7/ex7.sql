/*Team(team_id, team_name, home city, number of point accumulate, team_captain)
Player(player_ID, player name, age, nationality, team, position)*/


CREATE DATABASE IF NOT EXISTS soccer;
USE soccer;

DROP TABLE IF EXISTS soccer_Team;
CREATE TABLE IF NOT EXISTS soccer_Team(
  ID   INT              NOT NULL UNIQUE ,
  Name VARCHAR (50)     NOT NULL UNIQUE ,
  Home_City  VARCHAR(20)  NOT NULL UNIQUE ,
  Number_of_point INT,
  Team_Captain VARCHAR(30) NOT NULL UNIQUE ,
  PRIMARY KEY (Name)
);


DROP TABLE IF EXISTS soccer_Player;
CREATE TABLE IF NOT EXISTS soccer_Player(
  ID   INT              NOT NULL UNIQUE ,
  Name VARCHAR(30) NOT NULL UNIQUE ,
  Age INT NOT NULL,
  Nationality VARCHAR(20) NOT NULL,
  Team VARCHAR(50) NOT NULL,
  Position VARCHAR(20),
  PRIMARY KEY (Name)
);

INSERT INTO soccer_Team (ID, Name, Home_City, Number_of_point, Team_Captain) VALUES
  ('01', 'Lonely Valentines day', 'Nagasaki','99', 'Sakamoto Ryuoma'),
  ('02', 'Geek Tank World', 'Los Angeles', '108', 'Mark Zuckerberg'),
  ('03', 'Volcanic Weather', 'Auckland', '87', 'Phil Goff');

INSERT INTO soccer_Player (ID, Name, mche618.soccer_Player.Age, mche618.soccer_Player.Nationality, mche618.soccer_Player.Team, mche618.soccer_Player.Position) VALUES
  ('901', 'Sakamoto Ryuoma', 30, 'Japan', 'Lonely Valentines day', 'forward' ),
  ('902', 'Mark Zuckerberg', 27, 'USA', 'Geek Tank World', 'Goalie' ),
  ('903', 'Phil Goff', 60, 'New Zealand', 'Volcanic Weather', 'Guard' ),
  ('904', 'Donaly Trump', 45, 'USA', 'Lonely Valentines day', 'Guard' ),
  ('905', 'Harry Porter', 20, 'UK', 'Volcanic Weather', 'forward' );

ALTER TABLE soccer_Team
ADD CONSTRAINT FK_CAPTAIN
FOREIGN KEY (Team_Captain) REFERENCES soccer_Player (Name);

ALTER TABLE soccer_Player
ADD FOREIGN KEY (Team) REFERENCES soccer_Team(Name);

SELECT * FROM soccer_Player;


SELECT s.Name FROM soccer_Player AS s, soccer_Team AS t  WHERE  s.Team= t.Name AND t.Home_City = 'Nagasaki';